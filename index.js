const getCube = 2 ** 3
console.log(`The cube of 2 is ${getCube}`);

//----------------------------------------------------------

const address = [258, 'Washington Ave NW', 'California', 90011 ]
const [houseNumber, streetName, cityName, zipCode] = address;
console.log (`I live at ${houseNumber} ${streetName}, ${cityName} ${zipCode}`);

//----------------------------------------------------------

const animal ={
	name : 'Lolong',
	animalType: 'saltwater crocodile',
	height: '20 ft 3 in.',
	weight: 1075
}

console.log(`${animal.name} was a ${animal.animalType}. He weighed at ${animal.weight} kgs with a measurement of ${animal.height}`);

//------------------------------------------------------------

const numArray = [ 1, 2, 3, 4, 5];
numArray.forEach((numbers) =>console.log(numbers));


//---------------------------------------------------

class Dog{
	constructor(name, age, breed){
		this.Name = name;
		this.Age = age;
		this.Breed = breed;
	}
}

let myDog = new Dog();
myDog.Name = 'Kimbra';
myDog.Age = 5;
myDog.Breed = 'Terrier';
console.log(myDog);















